﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace Smarthome.Utility
{
    public static class TypeScriptInterfacesExtension
    {
        private static List<string> oneFile;

        private static Type[] nonPrimitivesExcludeList = new Type[]
        {
                typeof(object),
                typeof(string),
                typeof(decimal),
                typeof(void),
        };

        private static IDictionary<Type, string> convertedTypes = new Dictionary<Type, string>()
        {
            [typeof(string)] = "string",
            [typeof(char)] = "string",
            [typeof(byte)] = "number",
            [typeof(sbyte)] = "number",
            [typeof(short)] = "number",
            [typeof(ushort)] = "number",
            [typeof(int)] = "number",
            [typeof(uint)] = "number",
            [typeof(long)] = "number",
            [typeof(ulong)] = "number",
            [typeof(float)] = "number",
            [typeof(double)] = "number",
            [typeof(decimal)] = "number",
            [typeof(bool)] = "boolean",
            [typeof(object)] = "any",
            [typeof(void)] = "void",
        };

        /// <summary>
        /// Refactor C# Entities to Typescript - Looks at the Controllers
        /// </summary>
        /// <param name="app"></param>
        /// <param name="path">Destination path to the Generator output</param>
        public static void GenerateTypeScriptInterfaces(this IApplicationBuilder app, string path)
        {
            app.GenerateTypeScriptInterfaces(path, null, null);
        }

        /// <summary>
        /// Refactor C# Entities to Typescript - Looks at the path/EntitiesFolder
        /// </summary>
        /// <param name="app"></param>
        /// <param name="path">Destination path to the Generator output</param>
        /// <param name="destinationFolder">The C# Entity folder</param>
        public static void GenerateTypeScriptInterfaces(this IApplicationBuilder app, string path, string destinationFolder)
        {
            app.GenerateTypeScriptInterfaces(path, destinationFolder, null);
        }

        ///// <summary>
        ///// Refactor C# Entities to Typescript - Looks at the Controllers and Build a SingleFile
        ///// </summary>
        ///// <param name="app"></param>
        ///// <param name="path">Destination path to the Generator output</param>
        ///// <param name="singleFileTarget">The Output for OneFile Generation</param>
        //public static void GenerateTypeScriptInterfaces(this IApplicationBuilder app, string path, string singleFileTarget)
        //{
        //    app.GenerateTypeScriptInterfaces(path, null, singleFileTarget);
        //}

        /// <summary>
        /// Refactor C# Entities to Typescript - Looks at the path/EntitiesFolder and Build a SingleFile
        /// </summary>
        /// <param name="app"></param>
        /// <param name="path">Destination path to the Generator output</param>
        /// <param name="destinationFolder">The C# Entity folder</param>
        /// <param name="singleFileTarget">The Output for OneFile Generation</param>
        public static void GenerateTypeScriptInterfaces(this IApplicationBuilder app, string path,
        string destinationFolder,
        string singleFileTarget)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
            Type[] typesToConvert;
            if (destinationFolder != null)
                // get types from Folder
                typesToConvert = GetTypesToConvert(destinationFolder);
            else
                // get types from Assembly -> ControllerBase
                typesToConvert = GetTypesToConvert(Assembly.GetExecutingAssembly());

            foreach (Type type in typesToConvert)
            {
                // path shit
                var assemblyNamePath = GetAssemblyNames(type);
                CreatePath(path);

                // Classes
                if (type.Name != "IActionResult")
                {
                    var tsType = ConvertCs2Ts(type);
                    string fullPath = Path.Combine(path, tsType.Name.Replace(assemblyNamePath, ""));

                    ExportFile(fullPath, tsType.Lines, singleFileTarget);
                    // File.WriteAllLines(fullPath, tsType.Lines);
                }

                // baseClasses
                if (type.BaseType?.Name != null && type.BaseType.Name != "Object")
                {
                    var baseType = ConvertCs2Ts(type.BaseType);
                    var baseFullPath = Path.Combine(path, baseType.Name.Replace(assemblyNamePath, ""));

                    ExportFile(baseFullPath, baseType.Lines, singleFileTarget);
                    // File.WriteAllLines(baseFullPath, baseType.Lines);
                }
            }

            if (!string.IsNullOrEmpty(singleFileTarget))
                SaveSingleFile(path + singleFileTarget);
        }

        /// <summary>
        /// Saves all Entities in OneFile
        /// </summary>
        /// <param name="singleFileTarget"></param>
        private static void SaveSingleFile(string singleFileTarget)
        {
            if (!string.IsNullOrEmpty(singleFileTarget))
            {
                // import sorting
                var imports = oneFile.FindAll(e => e.Contains("import"));
                foreach (var item in imports)
                {
                    var index = oneFile.IndexOf(item);
                    oneFile.RemoveAt(index);

                    if (oneFile[index] == "")
                        oneFile.RemoveAt(index);
                }

                File.WriteAllLines(singleFileTarget, oneFile);
            }
        }

        /// <summary>
        /// Exports all Entities into a Singlefile
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="lines"></param>
        /// <param name="singleFileTarget"></param>
        private static void ExportFile(string fullPath, string[] lines, string singleFileTarget)
        {
            if (oneFile == null)
                oneFile = new List<string>();

            if (string.IsNullOrEmpty(singleFileTarget))
            {
                CreatePath(fullPath);
                File.WriteAllLines(fullPath, lines);
            }
            else
                oneFile.AddRange(lines);
        }

        private static string GetAssemblyNames(Type type)
        {
            var assemblyQualified = type.AssemblyQualifiedName.Split(',')[1].Trim().Split('.');
            string assemblyNamePath = "";
            foreach (var item in assemblyQualified)
            {
                assemblyNamePath += item + "/";
            }
            return assemblyNamePath;
        }

        private static void CreatePath(string path)
        {
            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        private static Type[] GetTypesToConvert(Assembly assembly)
        {
            Type controllerBaseType = typeof(Microsoft.AspNetCore.Mvc.ControllerBase);

            ISet<Type> actionAttributeTypes = new HashSet<Type>()
                {
                     typeof(Microsoft.AspNetCore.Mvc.HttpGetAttribute),
                     typeof(Microsoft.AspNetCore.Mvc.HttpPutAttribute),
                     typeof(Microsoft.AspNetCore.Mvc.HttpPostAttribute)
                };

            var controllers = assembly.GetTypes()
                 .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(controllerBaseType));

            var actions = controllers.SelectMany(c => c.GetMethods()
                 .Where(m => m.IsPublic && m.GetCustomAttributes().Any(a => actionAttributeTypes.Contains(a.GetType())))
            );

            var types = actions.SelectMany(m => new Type[1] { m.ReturnType }.Concat(m.GetParameters().Select(p => p.ParameterType)));

            return types
                 .Select(t => ReplaceByGenericArgument(t))
                 .Where(t => !t.IsPrimitive && !nonPrimitivesExcludeList.Contains(t))
                 .Distinct()
                 .ToArray();
        }

        private static Type[] GetTypesToConvert(string folderName)
        {
            var asm = Assembly.GetExecutingAssembly();
            var classes = asm.GetTypes().Where(p =>
                  p.FullName.Contains(folderName)
            ).ToList();

            List<Type> typesToRemove = new List<Type>();
            foreach (var item in classes)
            {
                Type[] nestedTypes = item.GetNestedTypes();

                foreach (var type in nestedTypes)
                {
                    typesToRemove.Add(type);
                }
            }

            foreach (var remove in typesToRemove)
            {
                classes.Remove(remove);
            }

            return classes
                 .Select(t => ReplaceByGenericArgument(t))
                 .Where(t => !t.IsPrimitive && !nonPrimitivesExcludeList.Contains(t))
                 .Distinct()
                 .ToArray();
        }

        private static Type ReplaceByGenericArgument(Type type)
        {
            if (type.IsArray)
            {
                return type.GetElementType();
            }

            if (!type.IsConstructedGenericType)
            {
                return type;
            }

            var genericArgument = type.GenericTypeArguments.First();

            var isTask = type.GetGenericTypeDefinition() == typeof(Task<>);
            var isActionResult = type.GetGenericTypeDefinition() == typeof(Microsoft.AspNetCore.Mvc.ActionResult<>);
            var isEnumerable = typeof(IEnumerable<>).MakeGenericType(genericArgument).IsAssignableFrom(type);

            if (!isTask && !isActionResult && !isEnumerable)
            {
                throw new InvalidOperationException();
            }

            if (genericArgument.IsConstructedGenericType)
            {
                return ReplaceByGenericArgument(genericArgument);
            }

            return genericArgument;
        }

        /// <summary>
        /// Converts the cs file to ts
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Lines"></param>
        /// <returns>ClassName and CodeLines</returns>
        private static (string Name, string[] Lines) ConvertCs2Ts(Type type)
        {
            string filename = $"{type.Namespace.Replace(".", "/")}/I{type.Name}.d.ts";

            Type[] types = GetAllNestedTypes(type);

            var lines = new List<string>();

            foreach (Type t in types)
            {
                lines.Add($"");

                if (t.IsClass || t.IsInterface)
                {
                    ConvertClassOrInterface(lines, t);
                }
                else if (t.IsEnum)
                {
                    ConvertEnum(lines, t);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            return (filename, lines.ToArray());
        }

        private static string GetDiffPath(Type type)
        {
            var destination = type.FullName.Split(".").ToList();
            var check = type.BaseType.FullName.Split(".").ToList();

            destination.RemoveAt(destination.Count() - 1);
            check.RemoveAt(check.Count() - 1);

            string one = "";
            string two = "";

            foreach (string item in destination)
            {
                one += item + "/";
            }
            foreach (string item in check)
            {
                two += item + "/";
            }

            var fin = one.Replace(two, "");

            if (fin.Split("/").Count() > 1)
                fin = Regex.Replace(fin, "([A-Za-z])\\w+", "..");
            else
                fin = "./";

            return fin;
        }

        private static void ConvertClassOrInterface(IList<string> lines, Type type)
        {
            var className = "I" + type.Name;

            // Generates the Information for the BaseClass
            IEnumerable<PropertyInfo> baseProperties = null;
            if (type.BaseType != null && type.BaseType.FullName != "System.Object")
            {
                var baseClassName = "I" + type.BaseType.Name;

                var fin = GetDiffPath(type);
                string import = $"import {{ {baseClassName} }} from \"{fin}{baseClassName}\"";

                if (lines.Count() > 2)
                {
                    if (lines.Count(e => Regex.IsMatch(e, "{ " + baseClassName + " }")) <= 0)
                    {
                        if (lines.Count(e => Regex.IsMatch(e, "import")) >= 1)
                            lines.RemoveAt(0);
                        lines.Insert(0, "");
                        lines.Insert(1, import);
                    }
                }
                else
                {
                    lines.Add(import);
                    lines.Add("");
                }

                lines.Add($"export interface {className} extends {baseClassName} {{");
                baseProperties = type.BaseType.GetProperties().Where(p => p.GetMethod.IsPublic);
            }
            else
            {
                // Without BaseClass
                lines.Add($"export interface {className} {{");
            }

            foreach (PropertyInfo property in type.GetProperties().Where(p => p.GetMethod.IsPublic))
            {
                if (baseProperties != null && baseProperties.Any(e => e.Name == property.Name)) continue;

                Type propType = property.PropertyType;
                Type arrayType = GetArrayOrEnumerableType(propType);
                Type nullableType = GetNullableType(propType);

                Type typeToUse = nullableType ?? arrayType ?? propType;

                var convertedType = ConvertType(typeToUse);

                string suffix = "";
                suffix = arrayType != null ? "[]" : suffix;
                suffix = nullableType != null ? " | null" : suffix;

                if (arrayType != null && arrayType.FullName != null &&
                    arrayType.FullName.Contains(Assembly.GetExecutingAssembly().FullName.Split(',')[0]))
                {
                    var importName = "I" + arrayType.Name;

                    var fin = GetDiffPath(type);
                    var import = $"import {{ {importName} }} from \"{fin}{importName}\"";

                    lines.Insert(1, import);
                }

                if (arrayType == null && propType.FullName != null &&
                    propType.FullName.Contains(Assembly.GetExecutingAssembly().FullName.Split(',')[0]))
                {
                    var importName = "I" + propType.Name;

                    var fin = GetDiffPath(type);
                    var import = $"import {{ {importName} }} from \"{fin}{importName}\"";

                    lines.Insert(1, import);
                }
                 
                lines.Add($"\t{CamelCaseName(property.Name)}: {convertedType}{suffix};");
            }

            lines.Add($"}}");
        }

        private static string ConvertType(Type typeToUse)
        {
            if (convertedTypes.ContainsKey(typeToUse))
            {
                return convertedTypes[typeToUse];
            }

            if (typeToUse.IsConstructedGenericType && typeToUse.GetGenericTypeDefinition() == typeof(IDictionary<,>))
            {
                var keyType = typeToUse.GenericTypeArguments[0];
                var valueType = typeToUse.GenericTypeArguments[1];
                return $"{{ [key: {ConvertType(keyType)}]: {ConvertType(valueType)} }}";
            }

            return "I" + typeToUse.Name;
        }

        private static void ConvertEnum(IList<string> lines, Type type)
        {
            var enumValues = type.GetEnumValues().Cast<int>().ToArray();
            var enumNames = type.GetEnumNames();

            lines.Add($"export enum {type.Name} {{");

            for (int i = 0; i < enumValues.Length; i++)
            {
                lines.Add($"\t{enumNames[i]} = {enumValues[i]},");
            }

            lines.Add($"}}");
        }

        private static Type[] GetAllNestedTypes(Type type)
        {
            return new Type[] { type }
                 .Concat(type.GetNestedTypes().SelectMany(nt => GetAllNestedTypes(nt)))
                 .ToArray();
        }

        private static Type GetArrayOrEnumerableType(Type type)
        {
            if (type.IsArray)
            {
                return type.GetElementType();
            }

            else if (type.IsConstructedGenericType)
            {
                Type typeArgument = type.GenericTypeArguments.First();

                if (typeof(IEnumerable<>).MakeGenericType(typeArgument).IsAssignableFrom(type))
                {
                    return typeArgument;
                }
            }

            return null;
        }

        private static Type GetNullableType(Type type)
        {
            if (type.IsConstructedGenericType)
            {
                Type typeArgument = type.GenericTypeArguments.First();

                if (typeArgument.IsValueType && typeof(Nullable<>).MakeGenericType(typeArgument).IsAssignableFrom(type))
                {
                    return typeArgument;
                }
            }

            return null;
        }

        private static string CamelCaseName(string pascalCaseName)
        {
            return pascalCaseName[0].ToString().ToLower() + pascalCaseName.Substring(1);
        }
    }
}
