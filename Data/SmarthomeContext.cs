﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Smarthome.Entities;

namespace Smarthome.Data
{
    public sealed class SmarthomeContext : DbContext
    {
        public IConfiguration Configuration { get; }

        public SmarthomeContext(DbContextOptions<SmarthomeContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
            this.Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseLazyLoadingProxies();

            options.UseSqlServer(Configuration.GetConnectionString("SmarthomeContext"),
                sqLiteOptions => { sqLiteOptions.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName); });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Room>()
                .HasMany(d => d.Devices)
                .WithOne(r => r.Room)
                .HasPrincipalKey(x => x.Id)
                .HasForeignKey(x => x.RoomId);
        }

        public DbSet<Room> Rooms { get; set; }
        public DbSet<Device> Devices { get; set; }
    }
}
