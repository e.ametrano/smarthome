﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Smarthome.Entities;
using Smarthome.Repositories;
using Smarthome.Utility;

namespace Smarthome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : BaseController<Device, EfCoreDeviceRepository>
    {
        private EfCoreDeviceRepository TestRepo { get; }
        public EfCoreRoomRepository RoomRepo { get; }
        public MQTTnet.AspNetCore.MqttHostedServer MqttHostedServer { get; }

        public DeviceController(EfCoreDeviceRepository repository, EfCoreRoomRepository roomRepo, MQTTnet.AspNetCore.MqttHostedServer mqttHostedServer) : base(repository)
        {
            TestRepo = repository;
            RoomRepo = roomRepo;
            MqttHostedServer = mqttHostedServer;
        }


        [HttpPost("[action]/{id}")]
        public async Task<ActionResult<Device>> AddTest(int id)
        {
            var test = new Device()
            {
                Name = "TestDevice",
                Description = "testdingen",
                IpAddress = "10.10.10.10",
                RoomId = id
            };

            await TestRepo.Add(test);
            return CreatedAtAction("Get", new { id = test.Id }, test);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Device>> GetTest(int id)
        {
            var test = new Device()
            {
                Name = "TestDevice",
                Description = "testdingen",
                IpAddress = "10.10.10.10",
                RoomId = id
            };


            string result = "";
            // Get the MQTT service from the service collection
            var clients = await MqttHostedServer.GetClientStatusAsync().ConfigureAwait(true);
            //MqttHostedServer.PublishAsync(MQTTnet.MqttApplicationMessage, );
            return CreatedAtAction("Get", new { id = result }, test);
        }

        //Here you can build a message and post it
    }
}