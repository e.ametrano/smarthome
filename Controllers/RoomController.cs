﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Smarthome.Entities;
using Smarthome.Repositories;

namespace Smarthome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : BaseController<Room, EfCoreRoomRepositoryCached>
    {
        private readonly EfCoreDeviceRepository _deviceRepository;
        private readonly EfCoreRoomRepository testRepo;

        public RoomController(EfCoreRoomRepositoryCached repository,
            EfCoreDeviceRepository deviceRepository) : base(repository)
        {
            _deviceRepository = deviceRepository;

            testRepo = repository;
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<Room>> AddTest()
        {
            _deviceRepository.GetOne(1).Result.RoomId = 1;
            await _deviceRepository.SaveAsync();

            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult<Room>> AddRoom()
        {
            var room = new Room()
            {
                Name = "mir egal",
                Description = "etst"
            };
            await testRepo.Add(room);

            return Ok();
        }


        // GET: api/[controller]
        // [Route("[action]")]
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<Device>>> GetAllDevicesPerRoom(string id)
        {
            var result = new List<Device>();
            var devices = await _deviceRepository.GetAll();


            return await Task.FromResult(new ActionResult<IEnumerable<Device>>((result)));
        }
    }
}