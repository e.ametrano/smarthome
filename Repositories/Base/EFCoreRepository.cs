﻿using Microsoft.EntityFrameworkCore;
using Smarthome.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Smarthome.Data;
using Smarthome.Entities.Interfaces;

namespace Smarthome.Repositories.Base
{
    public abstract class EfCoreRepository<TEntity, TContext> : IRepository<TEntity>
            where TEntity : class, IEntity
            where TContext : DbContext
    {
        private readonly TContext _context;

        protected EfCoreRepository(TContext context)
        {
            this._context = context;
        }

        // Add
        public async Task<TEntity> Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            await SaveAsync();
            return entity;
        }

        // Delete
        public async Task<TEntity> Delete(TEntity entity)
        {
            return await Delete(entity.Id);
        }

        public async Task<TEntity> Delete(int id)
        {
            return await Delete(id.ToString());
        }

        public async Task<TEntity> Delete(string id)
        {
            var entity = await _context.Set<TEntity>().FindAsync(id);
            if (entity == null)
                return null;

            _context.Set<TEntity>().Remove(entity);
            await SaveAsync();

            return entity;
        }

        public IEnumerable<TEntity> DeleteWhere(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> entities = _context.Set<TEntity>().Where(predicate);

            foreach (var entity in entities)
            {
                _context.Entry<TEntity>(entity).State = EntityState.Deleted;
            }

            return entities;
        }

        // Get All
        public virtual async Task<List<TEntity>> GetAll()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        // Get One
        public virtual async Task<TEntity> GetOne(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<TEntity> GetOne(string id)
        {
            int.TryParse(id, out int intId);
            return await GetOne(intId);
        }

        public async Task<TEntity> GetOne(Expression<Func<TEntity, bool>> predicate)
        {
            return await Task.Run(() => _context.Set<TEntity>().FirstOrDefault(predicate));
        }

        public async Task<TEntity> GetOne(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await Task.Run(() => query.Where(predicate).FirstOrDefault());
        }

        // Update
        public async Task<TEntity> Update(int id, TEntity entity)
        {
            return await Update(id.ToString(), entity);
        }

        public async Task<TEntity> Update(string id, TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await SaveAsync();
            return entity;
        }

        // Misc
        public async Task<IEnumerable<TEntity>> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await Task.Run(() => query.AsEnumerable());
        }

        public async Task<IEnumerable<TEntity>> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            return await Task.Run(() => _context.Set<TEntity>().Where(predicate));
        }

        public async Task<bool> SaveAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
