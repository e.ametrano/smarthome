﻿using Smarthome.Data;
using Smarthome.Entities;
using Smarthome.Repositories.Base;

namespace Smarthome.Repositories
{
    public class EfCoreDeviceRepository : EfCoreRepository<Device, SmarthomeContext>
    {
        public EfCoreDeviceRepository(SmarthomeContext context) : base(context)
        {
        }
        // We can add new methods specific to the device repository here in the future
    }
}