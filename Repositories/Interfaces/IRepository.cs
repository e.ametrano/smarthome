﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Smarthome.Entities.Interfaces;

namespace Smarthome.Repositories.Interfaces
{
    public interface IRepository<T> : IWrite<T>, IRead<T>, IReadAdvanced<T>
            where T : class, IEntity
    {
        Task<IEnumerable<T>> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> FindBy(Expression<Func<T, bool>> predicate);
        Task<bool> SaveAsync();
    }
}
