﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Smarthome.Repositories.Interfaces
{
    public interface IWrite<T>
    {
        Task<T> Add(T entity);

        Task<T> Update(int id, T entity);
        Task<T> Update(string id, T entity);

        Task<T> Delete(T entity);
        Task<T> Delete(int id);
        Task<T> Delete(string id);
        IEnumerable<T> DeleteWhere(Expression<Func<T, bool>> predicate);
    }
}
