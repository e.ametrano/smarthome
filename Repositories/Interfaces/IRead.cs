﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Smarthome.Repositories.Interfaces
{
    public interface IRead<T>
    {
        Task<List<T>> GetAll();

        Task<T> GetOne(int id);
        Task<T> GetOne(string id);
    }
}
