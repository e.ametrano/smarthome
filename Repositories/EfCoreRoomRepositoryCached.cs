﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Smarthome.Data;
using Smarthome.Entities;

namespace Smarthome.Repositories
{
    public class EfCoreRoomRepositoryCached : EfCoreRoomRepository
    {
        private readonly Caching<List<Room>> _cacheAll;
        private readonly Caching<Room> _cacheOne;

        public EfCoreRoomRepositoryCached(SmarthomeContext context) : base(context)
        {
            _cacheAll = new Caching<List<Room>>();
            _cacheOne = new Caching<Room>();
        }

        public override async Task<List<Room>> GetAll()
        {
            return await _cacheAll.GetOrCreate(this.ToString() + "GetAll", () => base.GetAll());
        }

        public override async Task<Room> GetOne(int id)
        {
            return await _cacheOne.GetOrCreate(this.ToString() + id.ToString(), () => base.GetOne(id));
        }
    }
}