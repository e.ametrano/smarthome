﻿using Smarthome.Data;
using Smarthome.Entities;
using Smarthome.Repositories.Base;
using Smarthome.Repositories.Interfaces;

namespace Smarthome.Repositories
{
    public class EfCoreRoomRepository : EfCoreRepository<Room, SmarthomeContext>
    {
        public EfCoreRoomRepository(SmarthomeContext context) : base(context)
        {
        }
        // We can add new methods specific to the Room repository here in the future
    }
}