using System;
using System.Buffers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Smarthome.Repositories;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Smarthome.CustomLogging;
using Smarthome.Data;
using Smarthome.Entities;
using Smarthome.Repositories.Base;
using Smarthome.Repositories.Interfaces;
using Smarthome.Utility;
using MQTTnet.AspNetCore;
using MQTTnet.Server;
using System.Threading.Tasks;
using MQTTnet.Protocol;
using MQTTnet;

namespace Smarthome
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(option =>
            {
                option.EnableEndpointRouting = false;
                //option.OutputFormatters.Clear();
                //option.OutputFormatters.Add(new JsonOutputFormatter(new JsonSerializerSettings()
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                //}, ArrayPool<char>.Shared));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddMvc().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            //services.AddCors(options =>
            //{
            //    // this defines a CORS policy called "default"
            //    options.AddPolicy("default", policy =>
            //    {
            //        policy.WithOrigins("http://localhost:4200")
            //            .AllowAnyMethod()
            //            .AllowAnyHeader()
            //            .AllowCredentials();
            //    });
            //});

            #region MQTT Configuration
            //string hostIp = Configuration["MqttOption:HostIp"];//IP address
            //int hostPort = int.Parse(Configuration["MqttOption:HostPort"]);//The port number
            //int timeout = int.Parse(Configuration["MqttOption:Timeout"]);//overtime time
            //string username = Configuration["MqttOption:UserName"];//username
            //string password = Configuration["MqttOption:Password"];//password

            //// Build configuration items
            //var optionBuilder = new MqttServerOptionsBuilder()
            //    .WithDefaultEndpointBoundIPAddress(System.Net.IPAddress.Parse(hostIp))
            //    .WithDefaultEndpointPort(hostPort)
            //    .WithDefaultCommunicationTimeout(TimeSpan.FromMilliseconds(timeout))
            //    .WithConnectionValidator(t =>
            //    {
            //        if (t.Username != username || t.Password != password)
            //        {
            //            t.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
            //        }
            //        t.ReturnCode = MqttConnectReturnCode.ConnectionAccepted;
            //    });
            //var option = optionBuilder.Build();

            ////Service injection
            //services
            //    .AddHostedMqttServer(option)
            //    .AddMqttConnectionHandler()
            //    .AddConnections();
            //this adds a hosted mqtt server to the services
            services.AddHostedMqttServer(builder => builder.WithDefaultEndpointPort(1883));

            //this adds tcp server support based on Microsoft.AspNetCore.Connections.Abstractions
            services.AddMqttConnectionHandler();

            //this adds websocket support
            services.AddMqttWebSocketServerAdapter();
            #endregion

            // dev stages and so on hier
            services.AddDbContext<SmarthomeContext>();

            services.AddScoped<EfCoreRoomRepository>();
            services.AddScoped<EfCoreRoomRepositoryCached>();
            services.AddScoped<EfCoreDeviceRepository>();
            //services.AddScoped(typeof(EfCoreRepository<Room, SmarthomeContext>));
            //services.AddScoped<IRead<Room>, EfCoreRoomRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                loggerFactory.AddFileLogger(a =>
                {
                    a.Path = Directory.GetCurrentDirectory() + @"\Log\Server\" + "Smarthome_" + System.DateTime.Today.ToString("yyyy-MM-dd") + ".log";
                    a.LogLevel = LogLevel.Warning;
                });

                loggerFactory.AddConsoleLogger(c =>
                {
                    c.LogLevel = LogLevel.Debug;
                    c.Color = ConsoleColor.Gray;
                });
                loggerFactory.AddConsoleLogger(c =>
                {
                    c.LogLevel = LogLevel.Information;
                    c.Color = ConsoleColor.Blue;
                });
                loggerFactory.AddConsoleLogger(c =>
                {
                    c.LogLevel = LogLevel.Error;
                    c.Color = ConsoleColor.Red;
                });
            }

            loggerFactory.AddFileLogger(a =>
            {
                a.Path = Directory.GetCurrentDirectory() + @"\Log\Server\" + "Smarthome_" + System.DateTime.Today.ToString("yyyy-MM-dd") + ".log";
                a.LogLevel = LogLevel.Error;
            });

            if (env.IsDevelopment())
            {
                const string tsDefinitionsPath = "ClientApp\\src\\entityGenerator";
                var tsDefinitionsFullPath = Path.Combine(env.ContentRootPath, tsDefinitionsPath);
                //var destFullPath = Path.Combine(env.ContentRootPath, "Entities\\");
                app.GenerateTypeScriptInterfaces(tsDefinitionsFullPath);

                app.UseCors(policy => policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Api with action",
                    template: "api/{controller}/{action}/{id?}");

                routes.MapRoute(
                    name: "DefaultApi",
                    template: "api/{controller}/{id?}");
            });

            ServiceLocator.Instance = app.ApplicationServices;

            //app.UseCors("default");

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    if (PingHost("localhost", 4200))
                        spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                    else
                        spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }

        private Action<EventArgs> StartedHandle()
        {
            Console.WriteLine("Start Handle");
            return null;// new Action<EventArgs>(); //  
        }

        //private async void StartedHandle()
        //{
        //    var msg = new MqttApplicationMessageBuilder().WithPayload("welcome to mqtt").WithTopic("start");

        //    while (true)
        //    {
        //        try
        //        {
        //         //   await server.PublishAsync(msg.Build());
        //            msg.WithPayload("Mqtt is still awesome at " + DateTime.Now);
        //        }
        //        catch (Exception e)
        //        {
        //            Console.WriteLine(e);
        //        }
        //        finally
        //        {
        //            await Task.Delay(TimeSpan.FromSeconds(2));
        //        }
        //    }
        //}

        public static bool PingHost(string hostUri, int portNumber)
        {
            try
            {
                using var client = new TcpClient(hostUri, portNumber);
                return true;
            }
            catch (SocketException)
            {
                return false;
            }
        }
    }
}
