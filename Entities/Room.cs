﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Smarthome.Entities
{
    public class Room : Entity
    {
        public Room()
        {
        }

        public string Name { get; set; }
        public string Floor { get; set; }
        public string Description { get; set; }
        public string ActiveDirectoryGroup { get; set; }

        public virtual ICollection<Device> Devices { get; set; } = new List<Device>();
    }
}
