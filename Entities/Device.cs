﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Smarthome.Entities
{
    public class Device : Entity
    {
        public Device()
        {
        }

        public string Name { get; set; }
        public string IpAddress { get; set; }
        public string Description { get; set; }

        public int RoomId { get; set; }
        public virtual Room Room { get; set; }

        // Selector whats my job
        // Triggert from
        // To Trigger ...
        // C# Interpreter for Custom Code 
        // TODO: Kategorie art des Devices
    }
}
