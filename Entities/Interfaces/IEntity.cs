﻿namespace Smarthome.Entities.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
