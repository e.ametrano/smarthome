﻿using Smarthome.Entities.Interfaces;

namespace Smarthome.Entities
{
    public class Entity : IEntity
    {
        public int Id { get; set; }
    }
}
