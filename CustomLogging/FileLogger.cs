﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Smarthome.CustomLogging
{
    public class FileLogger : ILogger
    {
        private readonly string _name;
        private readonly FileLoggerConfiguration _config;

        public FileLogger(string name, FileLoggerConfiguration config)
        {
            _name = name;
            _config = config;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel == _config.LogLevel;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            if (_config.EventId == 0 || _config.EventId == eventId.Id)
            {
                try
                {
                    using (var sourceStream = new FileStream(_config.Path,
                        FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                    {
                        using (var writer = new StreamWriter(sourceStream))
                        {
                            writer.WriteLine($"{DateTime.Now.ToLongTimeString()}");
                            writer.WriteLine($"{logLevel.ToString()} - {eventId.Id} - {_name}");
                            writer.WriteLine($"{formatter(state, exception)}");
                            writer.WriteLine(@"____________________________________________________________________________");
                            writer.Close();
                        }
                    }
                }
                catch (Exception)
                {
                  //  DiContainer.Container.Get<ILogger>().LogError(ex, "FileLogger - Log() FileAccess: " + ex.Message);
                }
            }
        }
    }
}
