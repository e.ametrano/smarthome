﻿using System;
using Microsoft.Extensions.Logging;

namespace Smarthome.CustomLogging
{
    public class ConsoleLoggerConfiguration
    {
        public LogLevel LogLevel { get; set; } = LogLevel.Warning;
        public int EventId { get; set; } = 0;
        public ConsoleColor Color { get; set; } = ConsoleColor.Yellow;
    }
}