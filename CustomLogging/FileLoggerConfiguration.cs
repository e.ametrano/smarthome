﻿using Microsoft.Extensions.Logging;

namespace Smarthome.CustomLogging
{
    public class FileLoggerConfiguration
    {
        public LogLevel LogLevel { get; set; } = LogLevel.Warning;
        public int EventId { get; set; } = 0;
        public string Path = string.Empty;
    }
}