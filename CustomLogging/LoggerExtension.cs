using System;
using Microsoft.Extensions.Logging;

namespace Smarthome.CustomLogging
{
	public static class LoggerExtension
	{
		public static ILoggerFactory AddConsoleLogger(this ILoggerFactory loggerFactory,
			 ConsoleLoggerConfiguration config)
		{
			loggerFactory.AddProvider(new ConsoleLoggerProvider(config));
			return loggerFactory;
		}

		public static ILoggerFactory AddConsoleLogger(this ILoggerFactory loggerFactory)
		{
			var config = new ConsoleLoggerConfiguration();
			return loggerFactory.AddConsoleLogger(config);
		}

		public static ILoggerFactory AddConsoleLogger(this ILoggerFactory loggerFactory,
			 Action<ConsoleLoggerConfiguration> configure)
		{
			var config = new ConsoleLoggerConfiguration();
			configure(config);
			return loggerFactory.AddConsoleLogger(config);
		}


		public static ILoggerFactory AddFileLogger(this ILoggerFactory loggerFactory,
			 FileLoggerConfiguration config)
		{
			loggerFactory.AddProvider(new FileLoggerProvider(config));
			return loggerFactory;
		}

		public static ILoggerFactory AddFileLogger(this ILoggerFactory loggerFactory)
		{
			var config = new FileLoggerConfiguration();
			return loggerFactory.AddFileLogger(config);
		}

		public static ILoggerFactory AddFileLogger(this ILoggerFactory loggerFactory,
			 Action<FileLoggerConfiguration> configure)
		{
			var config = new FileLoggerConfiguration();
			configure(config);
			return loggerFactory.AddFileLogger(config);
		}
	}
}
