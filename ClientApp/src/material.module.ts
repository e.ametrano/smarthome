import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule, MatSidenavModule, MatListModule, MatGridListModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatInputModule, MatProgressSpinnerModule, MatCardModule, MatMenuModule, MatIconModule, MatSnackBar } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
    imports: [MatSliderModule,
        MatSlideToggleModule,
        FlexLayoutModule,
        MatButtonModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        MatSnackBar,
        MatSidenavModule,
        MatGridListModule],
    exports: [MatSliderModule,
        MatSlideToggleModule,
        FlexLayoutModule,
        MatButtonModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatMenuModule,
        MatIconModule,
        MatListModule,
        MatSnackBar,
        MatSidenavModule,
        MatGridListModule]
})


export class MaterialModule { }
