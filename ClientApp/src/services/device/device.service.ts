import { Injectable } from "@angular/core";
import { BaseService } from "../base/base.service";
import { HttpClient } from "@angular/common/http";
import { IDevice } from '../../entityGenerator/Entities/IDevice';

@Injectable({
    providedIn: "root"
})
export class DeviceService extends BaseService<IDevice> {

    constructor(http: HttpClient) {
        super(http, "api/device");
    }

    // Custom Stuff ...
}
