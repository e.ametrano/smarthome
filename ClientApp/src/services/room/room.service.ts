import { Injectable } from "@angular/core";
import { BaseService } from "../base/base.service";
import { HttpClient } from "@angular/common/http";
import { IRoom } from '../../entityGenerator/Entities/IRoom';

@Injectable({
    providedIn: "root"
})
export class RoomService extends BaseService<IRoom> {

    constructor(http: HttpClient) {
        super(http, "api/room");
    }

    // Custom Stuff ...
}
