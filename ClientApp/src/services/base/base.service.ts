import { IRead } from "../interfaces/IRead";
import { IWrite } from '../interfaces/IWrite';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';
import { IEntity } from '../../entityGenerator/Entities/IEntity';


export abstract class BaseService<T extends IEntity> implements IWrite<T>, IRead<T> {
    private _route: string;

    private envUrl;

    constructor(private http: HttpClient, route: string) {
        this._route = route
        this.envUrl = environment.urlAddress;
    }

    getAll(): Promise<T[]> {
        return new Promise((resolve, reject) => {
            this.http.get(this.createCompleteRoute(this._route, this.envUrl)).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject);
        });
    }

    getOne(id: string): Promise<T> {
        return new Promise((resolve, reject) => {
            this.http.get(this.createCompleteRoute(this._route + "/" + id, this.envUrl)).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject);
        });
    }

    add(item: T): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.post(this.createCompleteRoute(this._route, this.envUrl), item, this.generateHeaders()).subscribe((response: any) => {
                resolve(response);
            }, reject);
        });
    }

    update(id: string, item: T): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.put(this.createCompleteRoute(this._route + "/" + id, this.envUrl), item, this.generateHeaders()).subscribe((response: any) => {
                resolve(response);
            }, reject);
        });
    }

    delete(id: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.delete(this.createCompleteRoute(this._route + "/" + id, this.envUrl)).subscribe((response: any) => {
                resolve(response);
            }, reject);
        });
    }

    private createCompleteRoute(route: string, envAddress: string) {
        return `${envAddress}/${route}/`;
    }

    private generateHeaders() {
        return {
            headers: new HttpHeaders({ 'Content-Type': "application/json" })
        }
    }
}
