import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from '../material.module';
import { AdminRoomsComponent } from "./admin/admin-rooms/admin-rooms.component";
import { AdminDevicesComponent } from "./admin/admin-devices/admin-devices.component";
import { AdminDashboardComponent } from "./admin/admin-dashboard/admin-dashboard.component";
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { RoomComponent } from "./room/room.component";
import { DeviceComponent } from "./device/device.component";
import { RoomsComponent } from '../app/rooms/rooms.component';
import { DevicesComponent } from './devices/devices.component';
import { DeviceService } from '../services/device/device.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { EnvironmentService } from '../services/base/environment.service';
import { FilterPipe } from '../shared/filter.pipe';
import { FormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
    declarations: [
        AppComponent,
        AdminDevicesComponent,
        AdminRoomsComponent,
        AdminDashboardComponent,
        DashboardComponent,
        RoomComponent,
        DeviceComponent,
        NavigationComponent,
        RoomsComponent,
        DevicesComponent,
        FilterPipe
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        FormsModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    //providers: [DeviceService],
    bootstrap: [AppComponent]
})
export class AppModule { }
