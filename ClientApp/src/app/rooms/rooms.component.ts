import { Component, OnInit, ViewChild } from '@angular/core';
import { IRoom } from '../../entityGenerator/Entities/IRoom';
import { RoomService } from '../../services/room/room.service';
import { MatGridList } from '@angular/material';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Router } from '@angular/router';


@Component({
    selector: 'app-rooms',
    templateUrl: './rooms.component.html',
    styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {
    @ViewChild('grid', { static: false }) grid: MatGridList;

    public gridByBreakpoint = {
        xl: 5,
        lg: 4,
        md: 3,
        sm: 2,
        xs: 1
    };

    public rooms: IRoom[];
    public roomsCopy: IRoom[];
    public search = "";

    constructor(private roomService: RoomService, private router: Router, private mediaObserver: MediaObserver) {
        this.rooms = [];
        this.roomsCopy = [];
    }

    ngOnInit() {
        this.mediaObserver.media$.subscribe((change: MediaChange) => {
            this.grid.cols = this.gridByBreakpoint[change.mqAlias];
        });

        this.roomService.getAll().then(result => {
            this.rooms = result;
            this.roomsCopy = result;
        });
        this.roomsCopy = this.rooms;
    }

    public roomClicked(room: IRoom) {
        let id = room.id;
        this.router.navigate(['/room/' + id]);
    }

    public applyFilter(filterValue) {
        let val = filterValue.toLowerCase();
        let searchColumns = ['name', 'description', `floor`];
        if (val.length > 0) {
            this.rooms = this.roomsCopy.filter(function (d) {
                let matchFound = false;
                for (let data of searchColumns) {
                    let value = "" + d[data];
                    if (value.toLowerCase().indexOf(val) !== -1 || !val) {
                        matchFound = true;
                        break;
                    }
                }
                return matchFound;
            });
        } else {
            this.rooms = this.roomsCopy;
        }
    }
}



//		public static IWebHostBuilder CreateDefaultBuilder<T>(string[] args, bool isDebug, bool useAuthentication = true)
//where T: class {
//			var result = new WebHostBuilder();
//if (isDebug && useAuthentication) {
//    result.UseHttpSys(options => {
//        options.Authentication.AllowAnonymous = false;
//        options.Authentication.Schemes = AuthenticationSchemes.NTLM;
//    });
//}
//else {
//    result.UseKestrel();
//}

//result.ConfigureAppConfiguration((hostingContext, config) => {
//    var env = hostingContext.HostingEnvironment;

//    config.AddCommandLine(args)
//        .SetBasePath(Directory.GetCurrentDirectory())
//        .AddJsonFile("serversettings.json", true)
//        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
//        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
//        .AddEnvironmentVariables();
//})
//    .ConfigureLogging((hostingContext, logging) => {
//        if (hostingContext.HostingEnvironment.IsDevelopment()) {
//            logging.AddConsole();
//            logging.AddDebug();
//        }
//        logging.AddFile(hostingContext);
//    })
//    .UseIISIntegration()
//    .UseDefaultServiceProvider((ContextBoundObject, options) => {
//        options.ValidateScopes = ContextBoundObject.HostingEnvironment.IsDevelopment();
//    })
//    .UseStartup<T>();
