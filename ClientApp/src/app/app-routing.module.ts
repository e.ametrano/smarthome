import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { RoomComponent } from "./room/room.component";
import { DeviceComponent } from "./device/device.component";
import { RoomsComponent } from './rooms/rooms.component';
import { DevicesComponent } from './devices/devices.component';


const routes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent },

    { path: "room/:id", component: RoomComponent },
    { path: "rooms", component: RoomsComponent },
    { path: "device/:id", component: DeviceComponent },
    //{ path: "devices/:id", component: DevicesComponent },
    { path: "devices", component: DevicesComponent }];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        useHash: true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
