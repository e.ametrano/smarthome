import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { RoomService } from '../../services/room/room.service';
import { IRoom } from '../../entityGenerator/Entities/IRoom';
import { DeviceService } from '../../services/device/device.service';
import { IDevice } from '../../entityGenerator/Entities/IDevice';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { MatGridList } from '@angular/material';

@Component({
    selector: "app-room",
    templateUrl: "./room.component.html",
    styleUrls: ["./room.component.scss"]
})
export class RoomComponent implements OnInit {
    @ViewChild('grid', { static: false }) grid: MatGridList;

    public gridByBreakpoint = {
        xl: 5,
        lg: 4,
        md: 3,
        sm: 2,
        xs: 1
    }


    public devices: IDevice[];
    public id: string;
    public room: IRoom;

    constructor(private route: ActivatedRoute, private router: Router, private mediaObserver: MediaObserver, private roomService: RoomService, private deviceService: DeviceService) { }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.devices = [];

        this.mediaObserver.media$.subscribe((change: MediaChange) => {
            this.grid.cols = this.gridByBreakpoint[change.mqAlias];
        });


        this.roomService.getOne(this.id).then(e => {
            this.room = e;
            this.deviceService.getAll().then(i => i.forEach((value, index, array) => {
                if (value.roomId === e.id)
                    this.devices.push(value);
            }));
        });
    }

    public deviceClicked(device: IDevice) {
        let id = device.id;
        this.router.navigate(['/device/' + id]);
    }

}
