import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DeviceService } from '../../services/device/device.service';
import { IDevice } from '../../entityGenerator/Entities/IDevice';
import { Router } from '@angular/router';
import { MatGridList } from '@angular/material';
import { MediaChange, MediaObserver } from '@angular/flex-layout';

@Component({
    selector: 'app-devices',
    templateUrl: './devices.component.html',
    styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {
    @ViewChild('grid', { static: false }) grid: MatGridList;

    public devices: IDevice[];
    public devicesCopy: IDevice[];

    public gridByBreakpoint = {
        xl: 5,
        lg: 4,
        md: 3,
        sm: 2,
        xs: 1
    }

    constructor(private deviceService: DeviceService, private router: Router, private mediaObserver: MediaObserver) {
        this.devices = [];
    }

    ngOnInit() {
        this.mediaObserver.media$.subscribe((change: MediaChange) => {
            this.grid.cols = this.gridByBreakpoint[change.mqAlias];
        });

        this.deviceService.getAll().then(result => {
            this.devices = result;
            this.devicesCopy = result;
        });
    }

    public deviceClicked(device: IDevice) {
        let id = device.id;
        this.router.navigate(['/device/' + id]);
    }

    // TODO: den filter so erweitern das auch nested classen properties gehen
    public applyFilter(filterValue) {
        let val = filterValue.toLowerCase();
        let searchColumns = ["name", "ipAddress", `description`, "roomId", "room.name" ];
        if (val.length > 0) {
            this.devices = this.devicesCopy.filter(function (d) {
                let matchFound = false;
                for (let data of searchColumns) {
                    let value = "" + d[data];
                    if (value.toLowerCase().indexOf(val) !== -1 || !val) {
                        matchFound = true;
                        break;
                    }
                }
                return matchFound;
            });
        } else {
            this.devices = this.devicesCopy;
        }
    }
}


// TODO: mit ID dann RaumId oder ohne dann alles ...
