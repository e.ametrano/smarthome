import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from '@angular/material';
import { SwUpdate, SwPush } from '@angular/service-worker';

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
    title = "Smarthome";
    update = false;

    constructor(private snackBar: MatSnackBar, public updates: SwUpdate, private swPush: SwPush) { }

    ngOnInit() {
        this.updates.available.subscribe(update => {
            this.update = true;
            const snack = this.snackBar.open('New version available. Load New Version?', 'Reload', {
                duration: 6000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: ['bg-success']
            });
            snack.onAction().subscribe(() => {
                this.updates.activateUpdate().then(() => document.location.reload());
            });
        });
        // Auf Updates prüfen
        this.updates.checkForUpdate();

        // push
        const key = 'BBc7Bb5f5';

        this.swPush.requestSubscription({
            serverPublicKey: key
        })
            .then(sub => {
                console.debug('Push Subscription', JSON.stringify(sub));
            },
                err => {
                    console.error('error registering for push', err);
                });
    }
}
