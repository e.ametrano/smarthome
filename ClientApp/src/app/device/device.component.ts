import { Component, OnInit } from "@angular/core";
import { DeviceService } from "../../services/device/device.service";
import { Router, ActivatedRoute, ParamMap, NavigationEnd } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap, filter } from 'rxjs/operators';
import { IDevice } from '../../entityGenerator/Entities/IDevice';

@Component({
    selector: "app-device",
    templateUrl: "./device.component.html",
    styleUrls: ["./device.component.scss"]
})
export class DeviceComponent implements OnInit {

    public device: IDevice;

    private navigationEnd: Observable<NavigationEnd>;
    private routePathParam: Observable<string>;

    public id: string;

    constructor(private route: ActivatedRoute, private router: Router, private deviceService: DeviceService) {
    }

    ngOnInit() {
      this.device = {} as IDevice;

        this.id = this.route.snapshot.params['id'];

        this.deviceService.getOne(this.id).then(e => this.device = e);
    }
}
