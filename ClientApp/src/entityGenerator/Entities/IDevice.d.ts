
import { IRoom } from "./IRoom"
import { IEntity } from "./IEntity"

export interface IDevice extends IEntity {
	name: string;
	ipAddress: string;
	description: string;
	roomId: number;
	room: IRoom;
}
