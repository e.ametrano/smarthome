
import { IDevice } from "./IDevice"
import { IEntity } from "./IEntity"

export interface IRoom extends IEntity {
	name: string;
	floor: string;
	description: string;
	activeDirectoryGroup: string;
	devices: IDevice[];
}
